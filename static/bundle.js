var tooltips = {"fwd": "Go forward", "left": "Turn counterclockwise", "right": "Turn clockwise", "back": "Move back", "pwr": "Power the robot on if it's in sleep mode",
"dist": "Single step moving distance", "angle": "Turn angle", "dock": "Go to dock for charging"}

function prepare_tooltips() {
  $.each(tooltips, function( index, value ) {
    $("#"+index).mouseover(function() {$("#tooltip").text(value);}).mouseleave(function() {$("#tooltip").text("");});
  });
  
}	//+ prepare_tooltips

function send_command(command, value) {
  value = value || 0;
  $.post("/do", {command:command, value:value}, function(data, status) {
  	//+ Updating status
  	data = JSON.parse(data);
  	if (data['lidar_on'] == '1') {$("#li").hide(); $("#lo").show(); } else {$("#lo").hide(); $("#li").show(); }
  });
}	//+ send_command

function prepare_actions() {
  $("#fwd").click(function() {send_command("fwd", $("#dist").val());});
  $("#back").click(function() {send_command("back", $("#dist").val());});
  $("#left").click(function() {send_command("ccw", $("#angle").val());});
  $("#right").click(function() {send_command("cw", $("#angle").val());});
  $("#pwr").click(function() {send_command("power", 1);});
  $("#reboot").click(function() {send_command("reboot", 1);});
  $("#li").click(function() {send_command("lidar", 1);});
  $("#lo").click(function() {send_command("lidar", 0);});
  $("#scan").click(function() {send_command("scan", 1);});
  $("#dock").click(function() {send_command("dock", 1);});
}	//+ prepare_actions

$(document).ready(function() {
  $("#lo").hide();
  prepare_tooltips();
  prepare_actions();
});


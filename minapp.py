#coding=utf-8
from flask import Flask, request, render_template
import json
import os
from PyRoomba.RoombaSCI import RoombaAPI
import sys
import time

RFCOMM_DEV = os.getenv("ROOMBA_RFCOMM", "/dev/ttyUSBRoomba" if sys.platform.startswith("linux") else "/dev/tty.Bluetooth-Incoming-Port") 
RFCOMM_BAUDRATE = 115200
PIN_LP = 4	# Lidar power pin
PIN_RP = 18	# Robot power pin

if sys.platform.startswith('linux'):
    # YOu need it to access low-level GPIO - pins that control power management features on the robot 
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_LP, GPIO.OUT)
    GPIO.setup(PIN_RP, GPIO.OUT)

app = Flask(__name__)

params = {
    "tf_speed": 0,
    "tf_angle": 0,
    "lidar_on": 0,
    "robot_on": 0,
    "map": "/static/map.png",
    "image": "/static/dump.png"
}
print "Initializing API..."
roomba = RoombaAPI(RFCOMM_DEV, RFCOMM_BAUDRATE)
print "Connecting to Roomba..."
roomba.connect()

@app.route('/')
def main():
    return render_template('index.html', controls=render_template('controls.htm'), content=render_template('content.htm'))

@app.route('/stop')
def stop():
    roomba.off()
    roomba.close()
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return "Stopped!"

@app.route('/do', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        command = request.form['command']
        value = request.form['value']
        if command == 'power':
            if sys.platform.startswith('linux'):
                GPIO.output(PIN_RP, 1)
                time.sleep(0.1)
                GPIO.output(PIN_RP, 1)
            roomba.safe()
        elif command == 'lidar':
            if sys.platform.startswith('linux'):
                GPIO.output(PIN_LP, 0 if GPIO.input(PIN_LP) else 1)
                params['lidar_on'] = GPIO.input(PIN_LP)
            else:
                params['lidar_on'] = 0 if params['lidar_on'] == 1 else 1
        elif command == 'fwd':
            roomba.drive(roomba.speed, -32768)
            time.sleep(0.1)
            roomba.stop()
        elif command == 'back':
            roomba.drive(roomba.speed * -1, -32768)
            time.sleep(0.1)
            roomba.stop()
        elif command == 'cw':
            roomba.drive(75, -1)
            time.sleep(0.27 * value / 10)
            roomba.stop()
        elif command == 'ccw':
            roomba.drive(75, 1)
            time.sleep(0.27)
            roomba.stop()
        elif command == 'dock':
            roomba.dock()
    else:
      error = 'This method can only be invoked via POST!'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return json.dumps(params)

if __name__ == '__main__':
    # print app.url_map  # some debug
    app.run(host='0.0.0.0', port=5100, debug=True)
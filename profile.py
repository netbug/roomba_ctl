# coding: utf-8
""" User profile management functions """
import sys
import ujson as json
from datetime import timedelta

from flask import Blueprint, url_for, render_template, redirect, request, current_app
from flask_cors import cross_origin
from flask_user import current_user
from geoip import geolite2
from sqlalchemy import over, func
from random_words import RandomNicknames

from models import *
from forms import UserProfileForm

reload(sys)
sys.setdefaultencoding('utf8')

blueprint = Blueprint('user_profile', __name__)
rn = RandomNicknames()

# Getting country from IP
def get_country(request):
    match = geolite2.lookup(request.remote_addr)
    if match is not None:
        return match.country
    return ''


# Getting timezone from IP
def get_timezone(request):
    match = geolite2.lookup(request.remote_addr)
    if match is not None:
        return match.timezone
    return 'UTC'


# Checks token supplied at every page requested
# Returns logged user id, or None if no cookie found
#
# TODO: add token to cookie if no + process this every time
def check_id(request):
    token = request.cookies.get('token', None)
    if token:
        uid = User.query.filter_by(token=token)
        if not uid or uid.count() == 0:
            # data below depends on OAuth provider
            first_name = request.args.get("first_name", None)
            if not first_name:
                first_name = rn.random_nick(gender="u")
            last_name = request.args.get("last_name", None)
            if not last_name:
                last_name = rn.random_nick(gender="u")

            username = "{0}-{1}".format(first_name, last_name)
            email = "{0}@anonymous.com".format(username)
            tz = get_timezone(request)
            s = User(token=token, username=username, email=email, timezone=tz,
                     first_name=first_name, last_name=last_name,
                     confirmed_at=datetime.now(), active=False)
            # active = False -- anon users
            db.session.add(s)
            db.session.commit()
            return s.id
        else:
            return uid.first().id
    # if current_user.is_authenticated():
    #     return current_user.id
    return None


"""
@api {get} /achievements.json Returns achievements for user
@apiName GetAchievements
@apiGroup GameBackend
@apiVersion 0.1.1

"""
@cross_origin()
@blueprint.route('/achievements.json')
def get_achievement():
    user_id = check_id(request)
    if not user_id:
        return json.dumps({"error": -5, "message": "Not authenticated correctly!"})
    ach_query = db.session.query(Achievements)\
        .join(Achievements.ach)\
        .filter(Achievements.user_id == user_id, Achievements.is_active == True)
    achievements = []
    if ach_query.count() > 0:
        append = achievements.append
        for ach in ach_query.all():
            append(ach.ach.to_json())
    return json.dumps({"achievements": achievements})


"""
@api {get} /init.json Call this method to init game session
@apiName InitSession
@apiGroup GameBackend
@apiVersion 0.1.1

"""
@cross_origin()
@blueprint.route('/init.json')
def init_game():
    user_id = check_id(request)
    if not user_id:
        return json.dumps({"error": -5, "message": "Not authenticated correctly!"})
    clean_sessions(user_id=user_id, force=True)
    # create a fake session to imitate start
    gs = GameSessionsWordOrder(user_id=user_id, src_id=-1, result=0, ev=-1)
    db.session.add(gs)
    db.session.commit()
    return json.dumps({"success": True})


"""
@api {get} /stats.json Returns user statistics and profile: total score, high score, accuracy, languages played, number of sessions, per-session stats (to be pruned). Must be called at the end of the round.
@apiName GetUserStats
@apiGroup GameBackend
@apiVersion 0.1.1

"""
@cross_origin()
@blueprint.route('/stats.json')
def get_stats():
    user_id = check_id(request)
    if not user_id:
        return json.dumps({"error": -5, "message": "Not authenticated correctly!"})
    message = clean_sessions(user_id=user_id, force=True)
    print "clean_sessions:", message
    # TODO: cache these data, why??
    stats = db.session.query(GameStatsWordOrder.accuracy, GameStatsWordOrder.score).filter_by(user_id=user_id)
    accuracy = {}
    total_score = 0
    for game in stats:
        total_score += game.score
        ac = json.loads(game.accuracy)
        for key in ac:
            if key not in accuracy:
                accuracy[key] = {
                    "correct": 0,
                    "attempts": 0
                }
            accuracy[key]["correct"] += ac[key]["correct"]
            accuracy[key]["attempts"] += ac[key]["attempts"]
    out = {}
    user = User.query.filter_by(id=user_id).first()
    if "score" in message:
        score = int(message["score"])
        if score > int(user.max_score):
            user.max_score = score
            db.session.add(user)
            db.session.commit()
            print "Max Score updated:", score

        # code-part below varies on database type
        if current_app.config["DATABASE_TYPE"] != "sqlite3":
            # especially Postgresql
            nearest_user = db.session.query(
                    User.id,
                    User.max_score,
                    over(
                        func.row_number(),
                        partition_by=User.id
                    )
                )\
                .order_by(User.max_score.desc())\
                .filter(User.max_score < score)
                # first()

            for user, row_number in nearest_user.all():
                print "{0}# {1}".format(row_number, user.max_score)
        else:
            # nearest_user = db.session.query(User.id, User.max_score)
            pass

        # print "Found user with id={0}, nearest score={1}".format(nearest_user.id, nearest_user.max_score)

        out.update(message)
    out.update({
        # "user": user.id,  # don't show our secret data
        "name": "{0} {1}".format(user.first_name, user.last_name),
        "total_games": stats.count(),
        "total_score": int(total_score),
        "max_score": user.max_score,
        "accuracy": accuracy
    })
    # load user achievements
    ach_query = db.session.query(Achievements)\
        .join(Achievements.ach)\
        .filter(Achievements.user_id==user_id, Achievements.is_active==True)
    if ach_query.count() > 0:
        achievements = []
        append = achievements.append
        for ach in ach_query.all():
            append(ach.ach.to_json())
        out.update({"achievements": achievements})
    # print json.dumps(out, indent=2, separators=(',', ': '))
    return json.dumps(out)


# prune multiple sentence submission events into single game session
def clean_sessions(user_id=None, force=False):
    if not user_id:
        raise Exception("clean_sessions: not authenticated correctly")
    sessions = GameSessionsWordOrder.query\
        .filter_by(user_id=user_id)\
        .order_by(GameSessionsWordOrder.ts.desc())  # last on the top
    # clean up only if the oldest session outdated!
    last_sess = sessions.first()
    if not last_sess:
        return {"error": 0, "message": "Nothing to merge!"}
    elif datetime.utcnow() - last_sess.ts < timedelta(seconds=current_app.config['SESSION_DURATION']) and not force:
        return {"error": 0, "message": "Current session is not ended yet"}
    timemin = last_sess.ts
    timemax = last_sess.ts
    score = 0
    accuracy = {}
    for sess in sessions:
        if sess.sentence_id != -1:
            lang = db.session.query(Sentence).filter(Sentence.id == sess.sentence_id).first().lang_id  # with is code actually, not Int...
            if lang not in accuracy:
                accuracy[lang] = {
                    "correct": 0,
                    "attempts": 0
                }
            accuracy[lang]["correct"] = accuracy[lang]["correct"]+1 if sess.result else accuracy[lang]["correct"]
            accuracy[lang]["attempts"] += 1

            timemin = min(sess.ts, timemin)
            timemax = max(sess.ts, timemin)
            score = score + sess.evaluation if sess.result else score

        db.session.delete(sess)
    # print "accuracy:", json.dumps(accuracy, indent=2, separators=(',', ': '))
    gs = GameStatsWordOrder(user_id=user_id, ts=timemin, time=(timemax - timemin).seconds,
                            accuracy=json.dumps(accuracy), score=score)
    db.session.add(gs)

    # TODO: remove progress on unfinished row-achievements if out of session

    db.session.commit()
    return {"score": score, "current_accuracy": accuracy}


# --------------------------------------------------------------------------------
# OLD undocumented stuff
@blueprint.route('/user/profile', methods=['GET', 'POST'])
def user_profile_page():
    form = UserProfileForm(request.form, current_user)
    if request.method is 'POST' and form.validate():
        form.populate_obj(current_user)  # Copy form fields to user_profile fields
        db.session.commit()  # Save user_profile
        return redirect(url_for('home_page'))  # Redirect to home page
    else:
        return render_template('dashboard/user_profile_page.html', form=form)  # Process GET or invalid POST

